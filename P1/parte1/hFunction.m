function [result] = hFunction(x,theta)
	m = length(x);
	x = [ones(m,1),x];

	thetaTrans = theta.';
	x = thetaTrans * x';
	result = x';
endfunction
