function [newTheta, cost] = descentGradient(x,y,alpha)
	m = length(y);
	newTheta = zeros(2,1);
	theta = zeros(2,1);
	theta(1,1) = 10;
	theta(2,1) = 4;
	f = @(x) (theta(1) + theta(2) .* x);	
	
	#plot(x, y, "marker", "x", "color", "red", "markersize", 6, "linestyle", "none");
	#hold;
	j = 1;
	for (i = [1:1500])
		#DEBUGGING
		if(i < 10)
			cost(:,j) = theta;
			j = j + 1;
		elseif(mod(i, 10) == 0)
			cost(:,j) = theta;
			j = j + 1;
		endif
		newTheta(1) = (theta(1)) - ((alpha/m) * sum((hFunction(x,theta))-y));
		newTheta(2) = (theta(2)) - ((alpha/m) * sum((hFunction(x,theta)-y).*x));
		theta = newTheta;
	
		#pause(0.1);
		#f = @(x) (theta(1) + theta(2) .* x);
		#plot(x,f(x));
		#hold;
	endfor
	#fplot(f,[5 25], 100);
	#hold;
	#plot(x, y, "x", "color", "red", "markersize", 6);
	#plot(x, @(x) (newTheta(1) + newTheta(2) .* x, x, y, "x", "color", "red", "markersize", 6);
endfunction
