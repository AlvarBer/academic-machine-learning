function [result] = hFunction(X,theta)

m = length(X(:,1));
thetaTrans = theta.';
X = thetaTrans * X';
result = X';
endfunction
