function idx = findClosestCentroids(X, centroids)
	m = rows(X);
	n = columns(X);
	k = rows(centroids);

	for i=1:m
		for j=1:k
			distances(j) = sqrt(sum((X(i,:) - centroids(j,:)) .^2));
		endfor
			[value, index] = min(distances);
			idx(i) = index;
	endfor
endfunction
