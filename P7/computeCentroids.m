function centroids = computeCentroids(X, idx, K)
	for i=1:K
		totalSum = sum(X(find(idx == i), :));
		Ck = length(find(idx == i));
		centroids(i,:) = (1/Ck) * totalSum;
	endfor
endfunction
