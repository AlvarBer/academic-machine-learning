# Part 1
#{
load("ex7data2.mat");

initial_centroids = [3 3; 6 2; 8 5];
max_iters = 10;
plot_progress = true;

runkMeans(X, initial_centroids, max_iters, plot_progress);
#}
# Part 2
bird = double(imread("bird_small.png"));
X = bird/255;

XReshaped = reshape(X, rows(X) * columns(X), 3);

max_iters = 20;
plot_progress = false;
k = 16;

#[centroids, idx] = runkMeans(XReshaped, randomCentroids(XReshaped, k), max_iters, plot_progress);

#idx = idx';

for i = 1:rows(idx)
	centroids(idx(i), :);
	compressed(i,:) = centroids(idx(i), :);
endfor

result = reshape(compressed, rows(X), columns(X), 3);
