# Show the precision of Monte Carlo method for the function x^2 + 25x for
# the interval [-1,1] and evaluating from 1 to 2000 points
 
fun = @(x)(-(x.^2) + 25 * x);
num_puntos = 5000;
a = 0;
b = 25;

#CAUTION!: IT TAKES LIKE 2 MINUTES IN CALCULATING IT
precisionFunction(fun,a,b,num_puntos);
