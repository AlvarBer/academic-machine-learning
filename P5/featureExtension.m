function out = featureExtension(X,p)

out = [X,ones(rows(X),p-1)];

for i = 2:p
    out(:, i) = out(:, i-1) .* X;
end

endfunction
