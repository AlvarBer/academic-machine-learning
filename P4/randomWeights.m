function weightMatrix = randomWeights (L_in, L_out)

INIT_EPSILON = sqrt(6) / sqrt(L_in + L_out);

weightMatrix = rand(L_in, 1 + L_out) * (2*INIT_EPSILON) - INIT_EPSILON;

endfunction