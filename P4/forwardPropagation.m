function [hypothesis] = forwardPropagation(X,Theta1,Theta2)

a1 = [ones(rows(X), 1), X];
z2 = (Theta1 * a1');
a2 = sigmoidFunction(z2);

a2 = [ones(1, columns(a2)); a2];
z3 = Theta2 * a2;
a3 = sigmoidFunction(z3);

hypothesis = a3';

endfunction
