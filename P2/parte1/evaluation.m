function [percentageHits] = evaluation(theta,X,y)

prediction = hFunction(X,theta);

m = length(y);
mask = ones(m,1) ./2;
prediction = prediction >= mask;
hits = sum(prediction == y);

percentageHits = hits/m * 100;

endfunction
