load("ex3data1.mat");
% Almacena los datos en X,y

m = size(X, 1);

% Selecciona aleatoriamente 100 ejemplos
rand_indices = randperm(m + 1);
sel = X(rand_indices(1:100),:);

# We can choose a single example too
#sel = X(1500,:);

# Compute theta for lambda = 1

#theta = oneVsAll(X,y,10,1);

# Here we cache and read the cached theta to save time debugging
#dlmwrite("cachedTheta", theta);
#theta = dlmread("cachedTheta");

# We can pass a selection and see the values the machine predicted
#displayData(sel);
#recognition(sel,theta);

#This function tells us the Accuracy of our regression logic
#display(["Accuracy of the logical-multi class regression is: " num2str(percentageAccuracy(X, y, theta)) "%"]);

# Graphics
lambdaVector = [.25,.5,1,2,3,4];
plotPercentages(X,y,lambdaVector);



