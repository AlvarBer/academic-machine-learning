load("ex3data1.mat");
# Stores the data in X,y

load("ex3weights.mat");
# Theta1 has a 25 x 401 dimension
# Theta2 has a 10 x 26 dimension

# We get the hypothesis throught fowardPropagation
hypothesis = fowardPropagation(X,Theta1,Theta2);

# And this functions tells us the accuracy of our neural network
display(["Neural network accuracy is: " num2str(percentageAccuracy(hypothesis, y)) "%"]);
